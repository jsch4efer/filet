pub mod filet {
    pub mod registry {
        use std::collections::HashMap;
        use glob::glob;
        use crypto::{sha2::Sha256, digest::Digest};

        pub trait Registry {
            fn run(&mut self, location: String, file_type: String) -> Result<(), String>;
        }
        pub struct InMemoryReg {
            hash_to_path: HashMap<String, String>,
            path_to_hash: HashMap<String, String>,
        }
        impl InMemoryReg {
            pub fn new() -> Self {
                Self {
                    hash_to_path: HashMap::default(),
                    path_to_hash: HashMap::default(),
                }
            }
        }
        impl Registry for InMemoryReg {
            fn run(&mut self, location: String, file_type: String) -> Result<(), String> {

                let pattern = format!("{}/**/*{}", location, file_type);
                for entry in glob(&pattern).unwrap() {
                    let file_path = String::from(entry.expect("Failed to read file").to_string_lossy());
                    let data = std::fs::read(&file_path).unwrap();
                    let mut hasher = Sha256::new();
                    hasher.input(&data[..]);
                    let hash = hasher.result_str().to_owned();

                    match self.path_to_hash.get(&file_path) {
                        Some(existing_hash) => {
                            if existing_hash.eq(&hash) {
                                println!("File already known and hasn't changed: {}", file_path);
                            } else {
                                println!("Found some differences for: {}", file_path);
                                self.path_to_hash.insert(file_path.clone(), hash.clone());
                                self.hash_to_path.insert(hash.clone(), file_path.clone());
                            }
                        }
                        None => {
                            match self.hash_to_path.get(&hash) {
                                Some(existing_file_path) => {
                                    if existing_file_path.eq(&file_path) {
                                        println!("File already known: {}", file_path);
                                    } else {
                                        println!("File already known under different path: {}", file_path);
                                        self.hash_to_path.insert(hash.clone(), file_path.clone());
                                    }
                                    self.path_to_hash.insert(file_path.clone(), hash.clone());
                                }
                                None => {
                                    println!("New file detected: {}", file_path);
                                    self.path_to_hash.insert(file_path.clone(), hash.clone());
                                    self.hash_to_path.insert(hash.clone(), file_path.clone());
                                }
                            }
                        }
                    }

                }
                let mut print = self.path_to_hash
                                    .iter()
                    .map(|(p, h)| format!("{} -> {}", p, h))
                    .collect::<Vec<String>>();
                print.sort();
                println!("Current registerd files:\n{}", print.join("\n"));
                return Ok(());
            }
        }
    }
}
