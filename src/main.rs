mod lib;

use clap::Parser;
use lib::filet::registry::{InMemoryReg, Registry};

/// A la carte file synchronization
#[derive(Parser, Debug)]
#[clap(author, version, about, long_about = None)]
pub struct Args {
    // /// Menu
    // #[clap(short, long)]
    // menu: String,
    /// Location
    #[clap(short, long)]
    location: String,

    /// File type
    #[clap(short, long)]
    file_type: String,
}

pub struct State {
    registry: InMemoryReg,
}


use std::{thread, time};

fn main() {
    let args = Args::parse();

    let mut state = State {
        registry: InMemoryReg::new(),
    };

    loop {
        println!("Synchronize files...");
        state.registry.run("".to_owned(), "".to_owned()).unwrap();
        let ten_millis = time::Duration::from_millis(1000);
        thread::sleep(ten_millis);
    }
}
