
use filet::filet::registry::{InMemoryReg, Registry};

fn main() {
    let mut registry = InMemoryReg::new();
    println!("==========");
    println!("First run:");
    registry.run("examples/pasta/first".to_owned(), "jpg".to_owned()).unwrap();
    println!("==========");
    println!("Second run:");
    registry.run("examples/pasta/second".to_owned(), "jpg".to_owned()).unwrap();
    println!("==========");
}
