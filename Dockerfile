FROM rust:latest as builder
WORKDIR /usr/src/filet
COPY . .
RUN cargo install --path .

FROM debian:buster-slim
COPY --from=builder /usr/local/cargo/bin/filet /usr/local/bin/filet
CMD ["filet"]
