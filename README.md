# Filet

A la carte file synchronization

## Getting started

Make sure to have a recent version of `Docker` and `docker-compose` on your system.
Just run `docker-compose build` to build and `docker-compose run filet` and run the application.

## Roadmap

- [X] Basic CLI interface
- [ ] File change detection
- [ ] Time-based scheduling 
- [ ] Specification for files of interest (duration, capacity, type)
- [ ] Communication protocol to synchronize files of interest 
- [ ] Secure connection of nodes
- [ ] Encryption of transit communication between nodes 
- [ ] Support for Windows 
- [ ] Support for Android 
- [ ] Support for iOS 

## License

This project is licensed under [GNU GPL v3.0](./LICENSE).
